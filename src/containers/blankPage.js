import React, { Component } from 'react';
import LayoutContentWrapper from '../components/utility/layoutWrapper';
import LayoutContent from '../components/utility/layoutContent';
import {
  Select, DatePicker, Input, Row, Col, Slider, Radio, Checkbox, Form, InputNumber, Switch, Button, Upload, Icon, Rate
} from 'antd';
import MyForm from './Form'
const Search = Input.Search;
const Option = Select.Option;
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const RadioGroup = Radio.Group;
const RadioButton = Radio.Button;
const plainOptions = ['Apple', 'Pear', 'Orange'];
const CheckboxGroup = Checkbox.Group;

const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}


export default class extends Component {
  state = {
    value: 1,
  }
  onChangeRadio = (e) => {
    console.log('radio checked', e.target.value);
    this.setState({
      value: e.target.value,
    });
  }

  render() {
    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      marginTop: '10px'
    };
    const divStyle = {
      marginTop: '20px'
    };

    return (
      <LayoutContentWrapper>
        <LayoutContent>
          <h1>Ant Design Components</h1>
          <div style={divStyle}>
            <h2>Inputs</h2>
            <Row style={rowStyle}>
              <Col md={5} sm={10} xs={10}>
                <Input size="large" placeholder="large size" />
              </Col>
            </Row>
            <Row style={rowStyle}>
              <Col md={5} sm={10} xs={10}>
                <Input placeholder="default size" />
              </Col>
            </Row>
            <Row style={rowStyle}>
              <Col md={5} sm={10} xs={10}>
                <Search
                  placeholder="input search text"
                  onSearch={value => console.log(value)}
                  enterButton
                />
              </Col>
            </Row>
            <Row style={rowStyle}>
              <Col md={5} sm={10} xs={10}>
                <Search
                  placeholder="input search text"
                  enterButton="Search"
                  size="large"
                  onSearch={value => console.log(value)}
                />
              </Col>
            </Row>
          </div>
          <div style={divStyle}>
            <h2>Select</h2>
            <Row style={rowStyle}>
              <Col md={5} sm={10} xs={10}>
                <Select
                  size='default'
                  defaultValue="a1"
                  onChange={this.handleChange}
                  style={{ width: 200 }}
                >
                  {children}
                </Select>
              </Col>
            </Row>
            <Row style={rowStyle}>
              <Col md={5} sm={10} xs={10}>
                <Select
                  size='large'
                  defaultValue="a1"
                  onChange={this.handleChange}
                  style={{ width: 200 }}
                >
                  {children}
                </Select>
              </Col>
            </Row>
            <Row style={rowStyle}>
              <Col md={5} sm={10} xs={10}>
                <Select
                  mode="multiple"
                  size='default'
                  placeholder="Please select"
                  defaultValue={['a10', 'c12']}
                  onChange={this.handleChange}
                  style={{ width: '100%' }}
                >
                  {children}
                </Select>
              </Col>
            </Row>
          </div>
          <div style={divStyle}>
            <h2>DatePicker</h2>
            <Row style={rowStyle}>
              <Col md={5} sm={10} xs={10}>
                <DatePicker onChange={this.onChange} />
              </Col>
            </Row>
            <Row style={rowStyle}>
              <Col md={5} sm={10} xs={10}>
                <DatePicker
                  showTime
                  placeholder="Select Time"
                  onChange={this.onChange}
                  onOk={this.onOk}
                />
              </Col>
            </Row>
            <Row style={rowStyle}>
              <Col md={5} sm={10} xs={10}>
                <RangePicker onChange={this.onChange} />
              </Col>
            </Row>
          </div>
          <div style={divStyle}>
            <h2>Slider</h2>
            <Row style={rowStyle}>
              <Col md={5} sm={10} xs={10}>
                <Slider range defaultValue={[20, 50]} />
              </Col>
            </Row>
            <Row style={rowStyle}>
              <Col md={5} sm={10} xs={10}>
                <Slider range step={10} defaultValue={[20, 50]} onChange={this.onChange} onAfterChange={this.onAfterChange} />
              </Col>
            </Row>
          </div>
          <div style={divStyle}>
            <h2>Radio Group</h2>
            <Row style={rowStyle}>
              <Col md={5} sm={10} xs={10}>
                <RadioGroup onChange={this.onChangeRadio} value={this.state.value}>
                  <Radio value={1}>A</Radio>
                  <Radio value={2}>B</Radio>
                  <Radio value={3}>C</Radio>
                  <Radio value={4}>D</Radio>
                </RadioGroup>
              </Col>
            </Row>
            <Row style={rowStyle}>
              <Col md={10} sm={10} xs={10}>
                <RadioGroup defaultValue="a">
                  <RadioButton value="a">Hangzhou</RadioButton>
                  <RadioButton value="b">Shanghai</RadioButton>
                  <RadioButton value="c">Beijing</RadioButton>
                  <RadioButton value="d">Chengdu</RadioButton>
                </RadioGroup>
              </Col>
            </Row>
            <Row style={rowStyle}>
              <Col md={5} sm={10} xs={10}>
              </Col>
            </Row>
          </div>
          <div style={divStyle}>
            <h2>CheckBox</h2>
            <Row style={rowStyle}>
              <Col md={5} sm={10} xs={10}>
                <CheckboxGroup options={plainOptions} defaultValue={['Apple']} onChange={this.onChangeCheckBox} />
              </Col>
            </Row>
          </div>
          <div style={divStyle}>
            <h2>Form</h2>
            <MyForm />
          </div>
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}
